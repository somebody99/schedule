package Application.Select;

import Application.Main.MainWindowController;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.io.*;
import java.util.HashMap;

public class SelectController {
    public Button cancel;

    public TextField text;

    private MainWindowController controller;
    private String type;

    public void OKButtonOnClick(MouseEvent event) {
        controller.setFilepath(text.getText() + ".txt");

        if (type == "open")
            open(text.getText() + ".txt");
        else
            saveAs(text.getText() + ".txt");

        Stage stage = (Stage) cancel.getScene().getWindow();
        stage.close();
    }

    public void CancelButtonOnClick(MouseEvent event) {
        Stage stage = (Stage) cancel.getScene().getWindow();
        stage.close();
    }

    public void setController(MainWindowController controller) {
        this.controller = controller;
    }

    public void setType(String type) {
        this.type = type;
    }

    public MainWindowController getController() {
        return controller;
    }

    public String getType() {
        return type;
    }

    protected void open(String filepath) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(filepath));
            String s = "";

            for (int i = 0; i < 7; ++i)
                controller.getSchedule().elementAt(i).clear();

            int i = 0;
            while ((s = reader.readLine()) != null) {
                if (s.charAt(0) >= '0' && s.charAt(0) <= '6') {
                    if (s.charAt(0) != '0')
                        ++i;

                    continue;
                }

                int index = s.indexOf(" ");
                controller.getSchedule().elementAt(i).put(s.substring(0, index), s.substring(index + 1, s.length()));
            }
            reader.close();
        } catch (FileNotFoundException e) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION,
                    "Такого файла не найдено :(", ButtonType.OK);

            alert.setTitle("Ошибка!");
            alert.setHeaderText(null);
            alert.setGraphic(null);

            alert.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void saveAs(String filepath) {
        try {
            FileWriter writer = new FileWriter(filepath, false);

            String s = "";
            for (int i = 0; i < 7; ++i) {
                s += String.valueOf(i) + "\n";

                if (controller != null)
                    for (HashMap.Entry entry : controller.getSchedule().elementAt(i).entrySet())
                        s += entry.getKey() + " " + entry.getValue() + "\n";
            }

            writer.write(s);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
package Application.Main;

import Application.Select.SelectController;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Vector;

public class MainWindowController {

    public Label day;

    public TextField lesson1;
    public TextField lesson2;
    public TextField lesson3;
    public TextField lesson4;
    public TextField lesson5;
    public TextField lesson6;
    public TextField lesson7;

    public MenuItem _new;
    public MenuItem open;
    public MenuItem save;
    public MenuItem saveAs;
    public MenuItem exit;
    public MenuItem about;

    private Vector<TextField> lessons = new Vector<TextField>();

    Vector<String> week;
    Vector<HashMap<String, String>> schedule;

    String filepath;

    @FXML
    public void initialize() {
        lessons.addElement(lesson1);
        lessons.addElement(lesson2);
        lessons.addElement(lesson3);
        lessons.addElement(lesson4);
        lessons.addElement(lesson5);
        lessons.addElement(lesson6);
        lessons.addElement(lesson7);

        week = new Vector<String>();
        week.addElement("Понедельник");
        week.addElement("Вторник");
        week.addElement("Среда");
        week.addElement("Четверг");
        week.addElement("Пятница");
        week.addElement("Суббота");

        schedule = new Vector<HashMap<String, String>>();
        for (int i = 0; i < 7; ++i)
            schedule.addElement(new HashMap<String, String>());

        if (filepath != "test.txt") {
            setShortCuts();

            filepath = "";
        }
    }

    public void NewOnAction(ActionEvent actionEvent) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION,
                "Вы уверены? Это сотрет все несохраненные данные!",
                ButtonType.OK, ButtonType.CANCEL);

        alert.setTitle("Осторожно!");
        alert.setHeaderText(null);
        alert.setGraphic(null);

        alert.showAndWait();

        if (alert.getResult() == ButtonType.CANCEL)
            return;

        filepath = "";

        for (HashMap hashMap : schedule)
            hashMap.clear();

        refresh(day.getText());
    }

    public void OpenOnActon(ActionEvent actionEvent) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getClassLoader().getResource("SelectWindow.fxml"));
            GridPane page = (GridPane) loader.load();

            SelectController controller = loader.getController();
            controller.setController(this);
            controller.setType("open");

            Stage dialogStage = new Stage();
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            dialogStage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }

        refresh(day.getText());
    }

    public void SaveOnAction(ActionEvent actionEvent) {
        if (filepath != "") {
            try {
                FileWriter writer = new FileWriter(filepath, false);

                String s = "";
                for (int i = 0; i < 7; ++i) {
                    s += String.valueOf(i) + "\n";

                    for (HashMap.Entry entry : schedule.elementAt(i).entrySet())
                        s += entry.getKey() + " " + entry.getValue() + "\n";
                }

                writer.write(s);
                writer.flush();
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION,
                    "Путь к файлу не задан.\nХотите задать?", ButtonType.OK, ButtonType.CANCEL);

            alert.setTitle("Внимание!");
            alert.setHeaderText(null);
            alert.setGraphic(null);

            alert.showAndWait();

            if (alert.getResult() == ButtonType.OK)
                SaveAsOnAction(actionEvent);
        }
    }

    public void SaveAsOnAction(ActionEvent actionEvent) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getClassLoader().getResource("SelectWindow.fxml"));
            GridPane page = (GridPane) loader.load();

            SelectController controller = loader.getController();
            controller.setController(this);
            controller.setType("saveAs");

            Stage dialogStage = new Stage();
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            dialogStage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void ExitOnAction(ActionEvent actionEvent) {
        Platform.exit();
    }

    public void AboutOnAction(ActionEvent actionEvent) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION,
                "Это приложение создано для составления учебного расписания, " +
                        "его хранения и дальнейшего пользования.\n" +
                        "В поля для текста необходимо ввести названия своих предметов.\n" +
                        "Чтобы перелистнуть день, используйте кнопки '<' и '>'.\n" +
                        "Не забудьте сохранить свое расписание :)", ButtonType.OK);

        alert.setTitle("О приложении");
        alert.setHeaderText(null);
        alert.setGraphic(null);

        alert.showAndWait();
    }

    public void LeftButtonOnClick(MouseEvent event) {
        int index = week.indexOf(day.getText());

        index = (index != 0) ? --index : (index = 5);

        day.setText(week.elementAt(index));

        refresh(index);
    }

    public void RightButtonOnClick(MouseEvent event) {
        int index = week.indexOf(day.getText());

        index = (index != week.size() - 1) ? ++index : (index = 0);

        day.setText(week.elementAt(index));

        refresh(index);
    }

    public void Typed1(KeyEvent keyEvent) {
        schedule.elementAt(0).put(day.getText(), lesson1.getText());
    }

    public void Typed2(KeyEvent keyEvent) {
        schedule.elementAt(1).put(day.getText(), lesson2.getText());
    }

    public void Typed3(KeyEvent keyEvent) {
        schedule.elementAt(2).put(day.getText(), lesson3.getText());
    }

    public void Typed4(KeyEvent keyEvent) {
        schedule.elementAt(3).put(day.getText(), lesson4.getText());
    }

    public void Typed5(KeyEvent keyEvent) {
        schedule.elementAt(4).put(day.getText(), lesson5.getText());
    }

    public void Typed6(KeyEvent keyEvent) {
        schedule.elementAt(5).put(day.getText(), lesson6.getText());
    }

    public void Typed7(KeyEvent keyEvent) {
        schedule.elementAt(6).put(day.getText(), lesson7.getText());
    }

    protected void refresh(int index) {
        for (int i = 0; i < 7; ++i)
            lessons.elementAt(i).setText(schedule.elementAt(i).get(week.elementAt(index)));
    }

    protected void refresh(String day) {
        for (int i = 0; i < 7; ++i)
            lessons.elementAt(i).setText(schedule.elementAt(i).get(day));
    }

    private void setShortCuts() {
        _new.setAccelerator(KeyCombination.keyCombination("Ctrl+N"));
        open.setAccelerator(KeyCombination.keyCombination("Ctrl+O"));
        save.setAccelerator(KeyCombination.keyCombination("Ctrl+S"));
        saveAs.setAccelerator(KeyCombination.keyCombination("Ctrl+Shift+S"));
        exit.setAccelerator(KeyCombination.keyCombination("Ctrl+Q"));
        about.setAccelerator(KeyCombination.keyCombination("Ctrl+A"));
    }

    public void setFilepath(String filepath){
        this.filepath = filepath;
    }

    public Vector<HashMap<String, String>> getSchedule() {
        return schedule;
    }

    public void setSchedule(Vector<HashMap<String, String>> schedule) {
        this.schedule = schedule;
    }
}
